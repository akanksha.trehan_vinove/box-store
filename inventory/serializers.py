from rest_framework import serializers

from inventory.models import Box, StoreUser


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = StoreUser
        fields = '__all__'

class AllBoxSerializer(serializers.ModelSerializer):
    area = serializers.SerializerMethodField()
    volume = serializers.SerializerMethodField()
    creator = serializers.SerializerMethodField()
    last_updated_ts = serializers.SerializerMethodField()


    class Meta:
        model = Box
        exclude = ('updated_by','updated_at','created_by', 'created_at')
        # fields = ['id','length','breadth','height','created_at','created_by','updated_at','updated_by', 'area', 'volume', 'creator', 'last_updated_ts']

    def get_area(self, ob):
        return (ob.length * ob.breadth)

    def get_volume(self, ob):
        return (ob.length * ob.breadth * ob.height)

    def get_creator(self, ob):
        if not self.context:
            return None
        if self.context['is_staff']:
            return (ob.created_by.user_id   )

    def get_last_updated_ts(self, ob):
        if not self.context:
            return None
        if self.context['is_staff']:
            return (ob.updated_at)


class AreaBoxSerializer(serializers.ModelSerializer):
    area = serializers.SerializerMethodField()
    volume = serializers.SerializerMethodField()

    class Meta:
        model = Box
        fields = ['area', 'volume']

    def get_area(self, ob):
        return (ob.length * ob.breadth)

    def get_volume(self, ob):
        return (ob.length * ob.breadth * ob.height)