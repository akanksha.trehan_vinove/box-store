import datetime

from django.contrib.auth import authenticate, login
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from inventory.models import StoreUser, Box
from inventory.serializers import AllBoxSerializer, AreaBoxSerializer
from inventory.utility import ErrorMessage, check_avg
from rest_framework.exceptions import ParseError, ValidationError
import time

from store.settings import MAX_AREA, MAX_VOLUME, BOXES_PER_WEEK, BOXES_PER_WEEK_USER


class Login(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        try:
            username = request.data['user_input']
            password = request.data['password']

            try:
                user_obj = StoreUser.objects.get(mobile=username)
            except StoreUser.DoesNotExist:
                raise ErrorMessage("Provide registered MobileEmail")

            user = authenticate(username=str(username), password=password)

            if user is not None:
                login(request, user)
                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)

                jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
                decoded_token = jwt_decode_handler(token)
                expirydate = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.localtime(decoded_token['exp']))

                resp_obj = {"data":{"is_user_login": True, "user_data": {"user_id": user_obj.mobile,
                                                                       "user_input": user_obj.first_name,
                                                                       "token": token, "token_date": expirydate}
                                     }}
                return Response(resp_obj, status=status.HTTP_200_OK)
            else:
                raise ErrorMessage("Provide valid Credentials")

        except (ParseError, KeyError, ValueError) as e:
            return Response({"data": {'isUserLogin': False, 'message': e.message},
                             "status": status.HTTP_200_OK,
                             "message": "success"}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({"message": "Exception which is not handled", "error": str(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class AddBox(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            user = request.user
            user_obj = StoreUser.objects.get(user_id=user)
            if not user_obj.is_staff:
                raise ErrorMessage("User has to be staff for this operation")
            input_length = int(request.data['length'])
            input_breadth = int(request.data['breadth'])
            input_height = int(request.data['height'])

            check_avg(input_length, input_breadth, input_height, user)
            #
            # # Average area and volume
            # box_list = Box.objects.all()
            # all_box_serializer = AreaBoxSerializer(box_list, many=True)
            # box_data = list(all_box_serializer.data)
            #
            # total_area = sum(item['area'] for item in box_data) if not box_data == [] else 0
            # input_area = input_length * input_breadth
            # average_area = (total_area + input_area) / (len(box_data) + 1)
            #
            # box_list_user = Box.objects.filter(created_by=user)
            # user_box_serializer = AreaBoxSerializer(box_list_user, many=True)
            # user_box_data = user_box_serializer.data
            # user_total_volume = sum(item['volume'] for item in box_data) if not user_box_data == [] else 0
            # user_input_volume = input_length * input_breadth * input_height
            # average_volume = (user_total_volume + user_input_volume) / (len(box_data) + 1)
            #
            # if average_area > MAX_AREA:
            #     raise ErrorMessage("Average area of all added boxes should not exceed " + str(MAX_AREA))
            # if average_volume > MAX_VOLUME:
            #     raise ErrorMessage(
            #         "Average volume of all boxes added by the current user should not exceed " + str(MAX_VOLUME))
            #
            # ref_time = datetime.datetime.now() + datetime.timedelta(days=-7)
            # total_boxes_queryset = Box.objects.filter(created_at__gte=ref_time).count()
            # total_boxes_user_queryset = Box.objects.filter(created_by=user, created_at__gte=ref_time).count()
            #
            # total_boxes_queryset = total_boxes_queryset + 1
            # total_boxes_user_queryset = total_boxes_user_queryset + 1
            #
            # if total_boxes_queryset > BOXES_PER_WEEK:
            #     raise ErrorMessage("Total Boxes added in a week cannot be more than " + str(BOXES_PER_WEEK))
            #
            # if total_boxes_user_queryset > BOXES_PER_WEEK_USER:
            #     raise ErrorMessage(
            #         "Total Boxes added in a week by a user cannot be more than " + str(BOXES_PER_WEEK_USER))

            Box.objects.create(length=input_length, breadth=input_breadth, height=input_height, created_by= user_obj)
            return Response(
                {"data": {"box_created": True, "message": "success"}}, status=status.HTTP_200_OK)

        except ErrorMessage as e:
            return Response(
                {"data": {"box_created": False, "message": "fail", "raw_message": e.message}}, status=status.HTTP_200_OK)
        except (ParseError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"data": {"box_created": False, "message": "fail", "raw_message": str(e)}}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UpdateBox(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, *args, **kwargs):
        try:
            user = request.user
            user_obj = StoreUser.objects.get(user_id=user)
            if not user_obj.is_staff:
                raise ErrorMessage("User has to be staff for this operation")

            input_box_id = int(request.data['box_id'])
            input_length = int(request.data['length'])
            input_breadth = int(request.data['breadth'])
            input_height = int(request.data['height'])

            check_avg(input_length, input_breadth, input_height, user)

            Box.objects.filter(id=input_box_id).update(length=input_length, breadth=input_breadth, height=input_height, updated_by= user_obj)
            return Response(
                {"data": {"box_updated": True, "message": "success"}}, status=status.HTTP_200_OK)

        except ErrorMessage as e:
            return Response(
                {"data": {"box_updated": False, "message": "fail", "raw_message": e.message}}, status=status.HTTP_200_OK)
        except (ParseError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"data": {"box_updated": False, "message": "fail", "raw_message": str(e)}}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ListAllBoxes(APIView):
    permission_classes = (AllowAny,)


    def get(self, request, *args, **kwargs):
        try:
            user = request.user
            user_obj = StoreUser.objects.get(user_id=user)
            is_staff = user_obj.is_staff

            filter_by = request.query_params.get('filter_by' ,None)
            filter_operation = request.query_params.get('filter_operation',None)
            filter_value = request.query_params.get('filter_value',None)

            if not (filter_by and filter_operation):
                box_list = Box.objects.all()
                serializer = AllBoxSerializer(box_list, many=True, context={'is_staff': is_staff})
                data = serializer.data
            else:
                all_filter_operation = {"less_than": "lt", "more_than": "gt", "before" : "lt", "after" : "gt", }
                all_filter = ["length", "breadth", "height", "area", "volume","created_at"]
                if filter_by in all_filter:
                    final_operation = "__"+all_filter_operation[filter_operation]
                else: #User as filter
                    user_obj = StoreUser.objects.get(mobile=filter_value)
                    filter_by = "created_by"
                    filter_value = user_obj.user_id
                    final_operation = ''

                _filter_query = {filter_by +final_operation : filter_value}
                box_list = Box.objects.filter(**_filter_query)
                serializer = AllBoxSerializer(box_list, many=True, context={'is_staff': is_staff})
                data = serializer.data

            return Response(
                {"data": {"boxes_list": data}}, status=status.HTTP_200_OK)

        except ErrorMessage as e:
            return Response(
                {"data": {"boxes_list":[], "message": "fail", "raw_message": e.message}}, status=status.HTTP_200_OK)
        except (ParseError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"data": {"boxes_list":[], "message": "fail", "raw_message": str(e)}}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ListMyBoxes(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            user = request.user
            user_obj = StoreUser.objects.get(user_id=user)
            is_staff = user_obj.is_staff
            if not user_obj.is_staff:
                raise ErrorMessage("User has to be staff for this operation")

            filter_by = request.query_params.get('filter_by', None)
            filter_operation = request.query_params.get('filter_operation', None)
            filter_value = request.query_params.get('filter_value', None)
            if not (filter_by and filter_operation):
                box_list = Box.objects.filter(created_by=user)
                serializer = AllBoxSerializer(box_list, many=True, context={'is_staff': is_staff})
                data = serializer.data
            else:
                all_filter_operation = {"less_than": "lt", "more_than": "gt", "before" : "lt", "after" : "gt", }
                all_filter = ["length", "breadth", "height", "area", "volume","created_at"]
                if filter_by in all_filter:
                    final_operation = "__"+all_filter_operation[filter_operation]
                else: #User as filter
                    user_obj = StoreUser.objects.get(mobile=filter_value)
                    filter_by = "created_by"
                    filter_value = user_obj.user_id
                    final_operation = ''

                _filter_query = {filter_by + final_operation: filter_value}
                box_list = Box.objects.filter(**_filter_query)

                serializer = AllBoxSerializer(box_list, many=True)#, context={'is_staff': is_staff})
                data = serializer.data

            return Response(
                {"data": {"boxes_list": data}}, status=status.HTTP_200_OK)

        except ErrorMessage as e:
            return Response(
                {"data": {"boxes_list":[], "message": "fail", "raw_message": e.message}}, status=status.HTTP_200_OK)
        except (ParseError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"data": {"boxes_list":[], "message": "fail", "raw_message": str(e)}}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class DeleteBox(APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            user = request.user
            user_obj = StoreUser.objects.get(user_id=user)
            if not user_obj.is_staff:
                raise ErrorMessage("User has to be staff for this operation")

            input_box_id = request.data['box_id']
            try:
                box_obj = Box.objects.get(id=input_box_id, created_by=user)
                box_obj.delete()
            except Box.DoesNotExist:
                raise ErrorMessage("This box cannot be deleted by you OR does not exist")
            return Response(
                {"data": {"boxes_deleted": True}}, status=status.HTTP_200_OK)

        except ErrorMessage as e:
            return Response(
                {"data": {"boxes_deleted": False, "message": "fail", "raw_message": e.message}}, status=status.HTTP_200_OK)
        except (ParseError, KeyError, ValueError, ValidationError):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(
                {"data": {"boxes_deleted": False, "message": "fail", "raw_message": str(e)}}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)