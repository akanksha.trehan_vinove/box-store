from django.urls import path

from inventory import views

urlpatterns = [

    path('login/', views.Login.as_view()),
    path('box/add/', views.AddBox.as_view()),
    path('box/update/', views.UpdateBox.as_view()),
    path('box/all/', views.ListAllBoxes.as_view()),
    path('box/all/user/', views.ListMyBoxes.as_view()),
    path('box/delete/', views.DeleteBox.as_view()),
]