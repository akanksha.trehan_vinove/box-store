import uuid

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class UserManager(BaseUserManager):
    # def create_user(self, password, email, mobile, last_name=None, first_name=None):
    #
    #     if mobile is None:
    #         raise TypeError('User must have a Mobile')
    #
    #     if email is None:
    #         raise TypeError('User must have an email address.')
    #
    #     user = self.model(first_name=first_name, last_name=last_name, email=self.normalize_email(email), mobile=mobile)
    #     user.set_password(password)
    #     user.save()
    #
    #     return user
    # def create_superuser(self, password, email, mobile, last_name=None, first_name=None, user_id=None):
    #     user = self.model(first_name=first_name, last_name=last_name, email=self.normalize_email(email), mobile=mobile,
    #                       user_id=user_id)
    #     user.set_password(password)
    #     user.is_superuser = True
    #     user.is_staff = True
    #     user.is_active = True
    #     user.save()
    #     return user
    def create_user(self, first_name, last_name, mobile, password):
        user = self.model(first_name=first_name, last_name=last_name, mobile=mobile)
        user.set_password(password)
        user.is_active = True
        user.save()
        return user

    def create_superuser(self, first_name, last_name, mobile, password):
        user = self.model(first_name=first_name, last_name=last_name, mobile=mobile)
        user.set_password(password)
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save()
        return user



class StoreUser(AbstractBaseUser, PermissionsMixin):
    user_id = models.CharField(primary_key=True, editable=True, default=uuid.uuid4, blank=False, unique=True,
                               max_length=500, name=("user_id"), verbose_name=("User ID"))
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    mobile = models.CharField(max_length=11, blank=True, null=True, unique=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    signup_date = models.DateField(blank=True, null=True)
    # jwt_secret = models.UUIDField(default=uuid.uuid4)
    last_login = models.DateTimeField(blank=True, null=True)


    USERNAME_FIELD = 'mobile'
    REQUIRED_FIELDS = ['first_name']

    objects = UserManager()

    def __str__(self):
        return "{}".format(self.user_id)

    def __user_id__(self):
        return self.user_id


    @property
    def token(self):
        return self._generate_jwt_token()

class Box(models.Model):
    id = models.IntegerField(primary_key=True)
    length = models.FloatField()
    breadth = models.FloatField()
    height = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(StoreUser, models.DO_NOTHING, related_name="cb")
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(StoreUser, models.DO_NOTHING, related_name="ub", null=True)
