import datetime

from inventory.models import Box
from inventory.serializers import AreaBoxSerializer
from store.settings import MAX_AREA, BOXES_PER_WEEK, BOXES_PER_WEEK_USER, MAX_VOLUME


class ErrorMessage(Exception):
    def __init__(self, message):
        self.message = message


def check_avg(input_length, input_breadth, input_height, user):
    # Average area and volume
    box_list = Box.objects.all()
    all_box_serializer = AreaBoxSerializer(box_list, many=True)
    box_data = list(all_box_serializer.data)

    total_area = sum(item['area'] for item in box_data) if not box_data == [] else 0
    input_area = input_length * input_breadth
    average_area = (total_area + input_area) / (len(box_data) + 1)

    box_list_user = Box.objects.filter(created_by=user)
    user_box_serializer = AreaBoxSerializer(box_list_user, many=True)
    user_box_data = user_box_serializer.data
    user_total_volume = sum(item['volume'] for item in box_data) if not user_box_data == [] else 0
    user_input_volume = input_length * input_breadth * input_height
    average_volume = (user_total_volume + user_input_volume) / (len(box_data) + 1)

    if average_area > MAX_AREA:
        raise ErrorMessage("Average area of all added boxes should not exceed " + str(MAX_AREA))
    if average_volume > MAX_VOLUME:
        raise ErrorMessage(
            "Average volume of all boxes added by the current user should not exceed " + str(MAX_VOLUME))

    ref_time = datetime.datetime.now() + datetime.timedelta(days=-7)
    total_boxes_queryset = Box.objects.filter(created_at__gte=ref_time).count()
    total_boxes_user_queryset = Box.objects.filter(created_by=user, created_at__gte=ref_time).count()

    total_boxes_queryset = total_boxes_queryset + 1
    total_boxes_user_queryset = total_boxes_user_queryset + 1

    if total_boxes_queryset > BOXES_PER_WEEK:
        raise ErrorMessage("Total Boxes added in a week cannot be more than " + str(BOXES_PER_WEEK))

    if total_boxes_user_queryset > BOXES_PER_WEEK_USER:
        raise ErrorMessage(
            "Total Boxes added in a week by a user cannot be more than " + str(BOXES_PER_WEEK_USER))
